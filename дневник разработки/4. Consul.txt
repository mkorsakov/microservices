От consul.io будем использовать только Service discovery и монитор состояния web UI for Consul.

Краткая информация о Consul. Consul - система обнаружения служб (Service Discovery). Состоит из агентов и серверов. Серверы могут входить в кластер, агенты подключаются (через join) к серверам.
Как у серверов, так и у агентов могут быть свои описания служб, которые включаются в единую базу и информация о них распределяется повсем серверам и агентам. Например, мы на одном узле держим БД и делаем его описание для consul-агента, после запуска агента информация о подключении к БД распространяется по всем узлам Consul. Как только агент останавливается, информация о его службах убирается из БД.
К consul можно обращаться как через собственный API, так и через собственный DNS-сервер, размещённый на порту 8600.

Т.к. основная цель - разработка микросервиса для виртуального склада, то мы и будем рассматривать всё на его примере. Микросервис "Товары" в нашей разработке побочный и нужен только для демонстрации взаимодействия микросервисов, будем считать, что он является законченным и трогать его не будем.

Для нашего микросервиса оптимально подходит следующая архитектура:
- основной хост энд-поинт, который исполняет роли:
  - Consul-сервер
  - сервер БД
  - nginx-балансировщик для узлов микросервиса
- один или несколько хостов с ролями:
  - узел микросервиса
  - consul-агент с описанием микросервиса
(- не нужно!) совместно с каждым consul-агентом установлен dnsmasq, который проксирует dns-запросы на Consul DNS-сервер.

Таким образом, каждый микросервис работает с локальным DNS-сервером и обращается к другим микросервисам по заранее определённым названиям. DNS-сервер Consul отдаёт информацию по всем хостам в случайном порядке, что позволяет балансировать нагрузку на хосты с одинаковыми микросервисами. Если ни один хост с микросервисом не доступен, DNS-сервер вернёт ошибку сопоставления имени с ip и доступ к микросервисам будет невозможен.

Что нужно сделать:
------------------

1. Конфиг для Consul-сервера
2. Конфиг для Consul-агента, проверить автоматический join
3. У микросервисов вынести настройки и данные о подключениях в отдельный файл, который генерировать через ansible-сценарий
4. Конфиг для nginx-балансировщика, основанный на consul-template

1. Создаём виртуалки для тестирования consul-сценариев. Первым делом создадим папку для разворачивания виртуалок через Vagrant vagrant/consul.
Скопируем в неё init_user.yml, который создавали на первом шаге. Чтобы этот сценарий применился к каждой виртуалке, изменим в нём одну строчку:
- hosts: all

Создаём Vagrantfile для инициализации двух ВМ: consul-сервер и onsul-агент:
# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"

  config.vm.provision "ansible" do |ansible|
    ansible.verbose = "v"
    ansible.playbook = "init_user.yml"
  end

  config.vm.define "n1" do |n1|
      n1.vm.hostname = "n1"
      n1.vm.network "private_network", ip: "192.168.44.21"
  end

  config.vm.define "n2" do |n2|
      n2.vm.hostname = "n2"
      n2.vm.network "private_network", ip: "192.168.44.22"
  end
end

Данный конфиг позволяет нам создать два узла n1 и n2. Запускать и останавливать ВМ можно как все сразу:
vagrant up
vagrant halt

Так и отдельно по конкретному узулу:
vagrant up n1
vagrant up n2
vagrant halt n1
vagrant halt n2

Подключаться к узлам через vagrant-ssh можно также указав узел:
vagrant ssh n1
vagrant ssh n2

Или как обычно, через ssh:
ssh 192.168.44.21
ssh 192.168.44.22

2. Создаём ansible-сценарий. Для разворачивания consul воспользуемся готовой ролью из galaxy: https://galaxy.ansible.com/savagegus/ansible-consul/
Добавим в requirements.txt (в папке с ansible-сценариями) строку:
- src: savagegus.consul

И выполним установку:
sudo ansible-galaxy install -r requirements.txt

Создадим отдельный playbook для тестирования consul test_consul.yml:
---
- name: Deploying consul server
  hosts: 192.168.44.21
  vars:
    consul_version: 0.7.0
    consul_is_server: true
    consul_datacenter: test
    consul_install_nginx: false
    consul_bootstrap: true
  become: yes
  roles:
    - savagegus.consul

- name: Deploying consul agent
  hosts: 192.168.44.22
  vars:
    consul_version: 0.7.0
    consul_datacenter: test
    consul_servers: ['192.168.44.21']
    consul_join_at_start: true
    consul_retry_join: true
    consul_install_nginx: false
  become: yes
  roles:
    - savagegus.consul

Чтоб не смешивать хосты, определим новый файл-инвентарь consul-hosts с содержимым:
192.168.44.21
192.168.44.22

Для запуска используем команду:
ansible-playbook test_consul.yml -i consul-hosts

Ни сервер, ни агент не поднялся, в логах есть следующая строка:
==> Starting Consul agent...
==> Error starting agent: Failed to get advertise address: Multiple private IPs found. Please configure one.

Связано с тем, что на виртуалке у нас несколько внешних интерфейсов и нужно указать конкретный адрес, на котором будет биндиться консул.
Пропишем в vars строку:
consul_bind_address: "{{ ansible_default_ipv4['address'] }}"

Не прокатило. Дело в том, что при создании виртуальной машины через vagrant с использованием провайдера virtual box, по умолчанию используется своя сеть, которая завязывается на интерфейс eth0. При настройке статического ip просто создаётся ещё один интерфейс eth1, которому присваивается указанный нами статический адрес. Ansible при определении ip адреса по умолчанию берёт первый роут до хоста 8.8.8.8, т.е. eth0, на этотже адрес и забиндится consul, а джойнится агент по адресу, который мы указываем в своей сети 192.168.44.21, отсюда и проблема.
Тогда укажем статический ip, который будет прослушивать агент:
consul_bind_address: 192.168.44.21
и
consul_bind_address: 192.168.44.22

Теперь можно зайти на любой виртуальный хост и выполнить команду:
consul members
В ответ выведется:
192.168.44.21  192.168.44.21:8301  alive   server  0.7.0  2         test
192.168.44.22  192.168.44.22:8301  alive   client  0.7.0  2         test

Ну и для полного счастья проверим consul ui.
Для этого в переменных для хоста 192.168.44.21 пропишем:
    consul_is_ui: true
    consul_install_nginx: true
    nginx_remove_default: yes

После прогона ansible-сценария, можно зайти через браузер в консоль consul-сервера:
http://192.168.44.21

На этом эксперименты с consul закончены, останавливаем виртуалки:
vagrant halt

Или можно удалить виртуалки:
vagrant destroy
