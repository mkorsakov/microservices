Предстоит реализовать три микросервиса:
- Пользователи
- Товары
- Склад
Можно начать с минимального микросервиса Товары.
Перед тем как создавать микросервисы, необходимо настроить нашу ОС. Я использую Ubuntu 14.04, для неё нет стандартной поставки python 3.5. Для начала разберёмся с этой проблемой.
Нашёл вот такой совет:
sudo add-apt-repository ppa:fkrull/deadsnakes
sudo apt-get update
sudo apt-get install python3.5
Сработало!

Возможно, стоит всё делать через сценарии ansible, чтоб в будущем не возвращаться к этим действиям?
Для этого создадим папочку под наши ansible-сценарии pyshop/ansible.
Создадим через vagrant виртуальную машину, на которой будем проверять ansible-сценарии.
Создадим каталог для ВМ vagrant/test и зайдём в него. Запустим инициализацию vagrant:
vagrant init ubuntu/trusty64
Откроем файл Vagrant и внесём туда строки:
  config.vm.network "private_network", ip: "192.168.44.10"
  config.vm.provision "ansible" do |ansible|
    ansible.verbose = "v"
    ansible.playbook = "init_user.yml"
  end
Это позволит нам задать статический ip и указать на ansible-сценарий для инициализации новой машины.
В файле init_user.yml описан сценарий, в котором создаётся пользователь с таким же именем, как и текущий пользователь в ОС и прописывается публичный ключ в authorized_keys для упрощённого входа. Это нам облегчит выполнение ansible-сценариев.
Содержимое файла init_user.yml:
---
- hosts: default

  vars:
    user: mgk
    public_key_path: "/home/mgk/.ssh/id_rsa.pub"

  tasks:
    - name: Create my user
      user:
          name: "{{ user }}"
          groups: admin,sudo
          append: yes
          shell: /bin/bash
      become: yes

    - name: Set sudo rights without password
      blockinfile:
          dest: /etc/sudoers
          block: |
            {{ user }} ALL=(ALL) NOPASSWD:ALL
      become: yes

    - name: Add authorized key
      authorized_key:
        user: "{{ user }}"
        key: "{{ lookup('file', '{{ public_key_path }}') }}"
      become: True
      become_user: "{{ user }}"

Запустим ВМ через команду vagrant up и попробуем подключиться под текущим пользователем с текущим ключом:
ssh 192.168.44.10

Теперь вернёмся к написанию основного ansible-сценария.
Создадим в папке pyshop/ansible файл main.yml, который будет основным нашим playbook-ом. Содержимое файла:
---
- name: Default playbook for deploying microservices
  hosts: virtual
  tasks:
    - include: "tasks/install_python3.5.yml"

В файле hosts опишем наш виртуальный хост, который мы создавали через ansible:
virtual ansible_ssh_host=192.168.44.10

В tasks/install_python3.5.yml пропишем сценарий для подключения нужного репозитория и установки python3.5:
---
- name: Add repository for non-official python versions
  apt_repository: repo='ppa:fkrull/deadsnakes'
  become: True

- name: Install python version 3.5
  apt: pkg=python3.5 update_cache=yes cache_valid_time=3600
  become: True

Пропишем настройки для ansible в файле ansible.cfg:
[defaults]
hostfile = hosts

retry_files_enabled = True
retry_files_save_path = .retry
allow_world_readable_tmpfiles = True

Проверим наш сценарий командой
ansible-playbook main.yml -C
И запустим сценарий на выполнение:
ansible-playbook main.yml

Подключимся к ВМ и проверим, что python3.5 установлен.
