import aiohttp

from models import get_goods_name, add_new_goods, update_goods_name


MS_GOODS_HOST = 'http://localhost:8080'


async def update_goods_info(conn, goods_id):
    goods_info = await request_goods_info(goods_id)
    await update_goods(conn, goods_info)


async def request_goods_info(goods_id):
    async with aiohttp.ClientSession() as session:
        url = '{}/goods/{}/'.format(MS_GOODS_HOST, goods_id)
        async with session.get(url) as resp:
            return await resp.json()


async def update_goods(conn, goods_info):
    goods_id = goods_info['id']
    name = goods_info['name']
    exist_goods_name = await get_goods_name(conn, goods_id)
    if exist_goods_name is None:
        await add_new_goods(conn, goods_id, name)
    elif name != exist_goods_name:
        await update_goods_name(conn, goods_id, name)
