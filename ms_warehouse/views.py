from aiohttp.web_reqrep import json_response

from models import get_goods_list, get_total_goods
from warehouse import warehouse_add, take


async def add_view(request):
    """
    :type request: aiohttp.web_reqrep.Request
    """
    post_data = await request.post()
    goods_id = int(post_data.get('goods_id'))
    volume = int(post_data.get('volume'))
    warehouse_add(goods_id=goods_id, volume=volume)
    return json_response()


async def take_view(request):
    """
    :type request: aiohttp.web_reqrep.Request
    """
    post_data = await request.post()
    goods_id = int(post_data.get('goods_id'))
    volume = int(post_data.get('volume'))
    take(goods_id=goods_id, volume=volume)
    return json_response()


async def warehouse_view(request):
    get_params_info = {
        'page': int,
        'items_on_page': int,
        'order_by': str,
    }

    params = {}
    for param, param_type in get_params_info.items():
        value = request.GET.get(param)
        if value is not None:
            params[param] = param_type(value)

    result = {
        'items': await get_goods_list(**params),
        'total': await get_total_goods(),
    }
    return json_response(result)
