from asyncio.coroutines import coroutine
from unittest import mock

from aiohttp.test_utils import AioHTTPTestCase

from app import init_app


class MyBaseTestCase(AioHTTPTestCase):
    def get_app(self, loop):
        return init_app(loop)

    def request(self, method, path, params=None, data=None):
        request = self.loop.run_until_complete(self.client.request(method, path, params=params, data=data))
        self.assertEqual(request.status, 200)
        content = self.loop.run_until_complete(request.json())
        return content


class WarehouseAddTestCase(MyBaseTestCase):
    @mock.patch('views.warehouse_add', autospec=True)
    def test_common(self, add_mock):
        add_mock.side_effect = coroutine(lambda goods_id, volume: None)
        goods_id = 10
        volume = 200

        self.request("POST", "/warehouse/add/", data={'goods_id': goods_id, 'volume': volume})

        add_mock.assert_called_once_with(goods_id=goods_id, volume=volume)
