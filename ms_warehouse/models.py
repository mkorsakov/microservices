import asyncpg


async def get_db_connection():
    conn = await asyncpg.connect(
        user='ms_warehouse',
        password='ms_warehouse',
        database='ms_warehouse',
        host='192.168.44.10'
    )
    return conn


async def get_warehouse_id_by_goods(conn, goods_id):
    return await conn.fetchval('SELECT id FROM warehouse WHERE goods_id=$1', goods_id)


async def insert_into_warehouse(conn, goods_id, volume):
    await conn.fetch('INSERT INTO warehouse (goods_id, volume) VALUES ($1, $2)', goods_id, volume)


async def update_warehouse(conn, w_id, volume):
    await conn.fetch('UPDATE warehouse SET volume = volume + $2 WHERE id=$1', w_id, volume)


async def get_goods_name(conn, goods_id):
    return await conn.fetchval('SELECT name FROM goods WHERE id=$1', goods_id)


async def add_new_goods(conn, goods_id, name):
    await conn.fetch('INSERT INTO goods (id, name) VALUES ({}, \'{}\')'.format(goods_id, name))


async def update_goods_name(conn, goods_id, name):
    await conn.fetch('UPDATE goods SET name=\'{}\' WHERE id={}'.format(name, goods_id))


async def get_warehouse_info_by_goods(conn, goods_id):
    return await conn.fetchrow('SELECT * FROM warehouse WHERE goods_id=$1', goods_id)

async def get_goods_list(page=0, items_on_page=20, order_by=None):
    """
    Получение списка из id и volume имеющихся товаров
    :type page: int
    :type items_on_page: int
    :type order_by: str
    :rtype: list of int
    """
    conn = await get_db_connection()
    offset = page * items_on_page
    sql_str = 'SELECT w.goods_id as goods_id, w.volume as volume, g.name as name FROM warehouse w ' \
              'LEFT JOIN goods g on g.id = w.goods_id'
    if order_by is not None:
        if order_by[0] == '-':
            order_by = order_by[1:]
            desc = True
        else:
            desc = False
        sql_str += ' ORDER BY {}'.format(order_by)
        if desc:
            sql_str += ' DESC'
    sql_str += ' LIMIT $1 OFFSET $2'
    sql_data = [items_on_page, offset]

    values = await conn.fetch(sql_str, *sql_data)

    await conn.close()
    return [{'id': item['goods_id'], 'volume': item['volume']} for item in values]


async def get_total_goods():
    """
    Получение общего количества товаров
    :rtype: int
    """
    conn = await get_db_connection()
    count = await conn.fetchval('SELECT COUNT(*) FROM warehouse')
    await conn.close()

    return count
