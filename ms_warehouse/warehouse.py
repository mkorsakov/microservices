from exceptions import NoGoods, NotEnoughGoods
from goods import update_goods_info
from models import get_db_connection, get_warehouse_id_by_goods, insert_into_warehouse, update_warehouse, \
    get_warehouse_info_by_goods


async def warehouse_add(goods_id, volume):
    conn = await get_db_connection()
    async with conn.transaction():
        await update_goods_info(conn, goods_id)
        await update_warehouse_item(conn, goods_id, volume)


async def take(goods_id, volume):
    conn = await get_db_connection()
    async with conn.transaction():
        w_info = await get_warehouse_info_by_goods(conn, goods_id)
        if w_info is None:
            raise NoGoods('Нет товара на складе!')
        else:
            new_volume = w_info['volume'] - volume
            if new_volume < 0:
                raise NotEnoughGoods('На складе недостаточно товара!')
            await update_warehouse(conn, w_info['id'], new_volume)


async def update_warehouse_item(conn, goods_id, volume):
    w_id = await get_warehouse_id_by_goods(conn, goods_id)
    if w_id is None:
        await insert_into_warehouse(conn, goods_id, volume)
    else:
        await update_warehouse(conn, w_id, volume)
