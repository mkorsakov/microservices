CREATE TABLE warehouse
(
  id SERIAL PRIMARY KEY,
  goods_id INTEGER,
  volume INTEGER -- Количество
);

CREATE TABLE goods
(
  id INTEGER NOT NULL PRIMARY KEY UNIQUE,
  name VARCHAR(200)
);

CREATE TABLE history
(
  id SERIAL PRIMARY KEY,
  datetime TIMESTAMP WITH TIME ZONE,
  warehouse_id INTEGER,
  volume INTEGER
);
