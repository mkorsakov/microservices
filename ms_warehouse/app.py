from aiohttp import web

from views import add_view, take_view, warehouse_view


def init_app(loop=None):
    app = web.Application(loop=loop)
    app.router.add_route('GET', r'/warehouse/', warehouse_view)
    app.router.add_route('POST', r'/warehouse/add/', add_view)
    app.router.add_route('POST', r'/warehouse/take/', take_view)
    return app
