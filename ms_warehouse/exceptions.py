class WarehouseException(Exception):
    pass


class NoGoods(WarehouseException):
    pass


class NotEnoughGoods(WarehouseException):
    pass
