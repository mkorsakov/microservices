import asyncpg


async def get_goods_id_list(page=0, items_on_page=20, order_by=None):
    """
    Получение списка id имеющихся товаров
    :type page: int
    :type items_on_page: int
    :type order_by: str
    :rtype: list of int
    """
    conn = await get_db_connection()
    offset = page * items_on_page
    sql_str = 'SELECT id FROM goods'
    if order_by is not None:
        if order_by[0] == '-':
            order_by = order_by[1:]
            desc = True
        else:
            desc = False
        sql_str += ' ORDER BY {}'.format(order_by)
        if desc:
            sql_str += ' DESC'
    sql_str += ' LIMIT $1 OFFSET $2'
    sql_data = [items_on_page, offset]

    values = await conn.fetch(sql_str, *sql_data)

    await conn.close()
    return [item['id'] for item in values]


async def get_total_goods():
    """
    Получение общего количества товаров
    :rtype: int
    """
    conn = await get_db_connection()
    count = await conn.fetchval('SELECT COUNT(*) FROM goods')
    await conn.close()

    return count


async def get_db_connection():
    conn = await asyncpg.connect(
        user='ms_goods',
        password='ms_goods',
        database='ms_goods',
        host='192.168.44.10'
    )
    return conn


async def get_goods_one_info(id):
    """
    Получение информации о товаре по id
    """
    conn = await get_db_connection()
    row = await conn.fetchrow('SELECT id, name FROM goods WHERE id={}'.format(id))
    await conn.close()

    return {'id': row['id'], 'name': row['name']}


async def get_goods_info(ids):
    """
    Получение информации о товарах по списку идентификаторов
    """
    conn = await get_db_connection()
    rows = await conn.fetch('SELECT id, name FROM goods WHERE id IN ({})'.format(','.join(ids)))
    await conn.close()

    return [{'id': row['id'], 'name': row['name']} for row in rows]
