from aiohttp.web_reqrep import json_response

from models import get_goods_id_list, get_total_goods, get_goods_one_info, get_goods_info


async def goods_view(request):
    get_params_info = {
        'page': int,
        'items_on_page': int,
        'order_by': str,
    }

    params = {}
    for param, param_type in get_params_info.items():
        value = request.GET.get(param)
        if value is not None:
            params[param] = param_type(value)

    result = {
        'items': await get_goods_id_list(**params),
        'total': await get_total_goods(),
    }
    return json_response(result)


async def goods_one_info(request):
    """
    :type request: aiohttp.web_reqrep.Request
    """
    id = int(request.match_info['id'])
    info = await get_goods_one_info(id)
    result = format_goods_info(info)
    return json_response(result)


async def goods_get_info(request):
    ids_str = request.GET.get('ids', '')
    ids = ids_str.split(',')
    info = await get_goods_info(list(ids))
    result = {'items_info': [format_goods_info(item) for item in info]}
    return json_response(result)


def format_goods_info(item):
    return {
        'id': item['id'],
        'name': item['name'],
    }
