from asyncio.coroutines import coroutine
from unittest import mock

from aiohttp.test_utils import AioHTTPTestCase

from app import init_app


class MyBaseTestCase(AioHTTPTestCase):
    def get_app(self, loop):
        return init_app(loop)

    def request(self, method, path, params=None):
        request = self.loop.run_until_complete(self.client.request(method, path, params=params))
        self.assertEqual(request.status, 200)
        content = self.loop.run_until_complete(request.json())
        return content


class GoodsTestCase(MyBaseTestCase):
    @mock.patch('views.get_goods_id_list', autospec=True)
    @mock.patch('views.get_total_goods', autospec=True)
    def test_common(self, get_total_goods_mock, get_goods_id_list_mock):
        get_goods_id_list_mock.side_effect = coroutine(lambda: [1, 2, 3])
        get_total_goods_mock.side_effect = coroutine(lambda: 3)

        content = self.request("GET", "/goods/")

        expected_data = {
            'items': [1, 2, 3],
            'total': 3,
        }
        self.assertEqual(content, expected_data)

    @mock.patch('views.get_goods_id_list', autospec=True)
    @mock.patch('views.get_total_goods', autospec=True)
    def test_page_and_items_on_page(self, get_total_goods_mock, get_goods_id_list_mock):
        """
        :type get_goods_id_list_mock: unittest.mock.MagicMock
        """
        get_goods_id_list_mock.side_effect = coroutine(lambda page, items_on_page: [1, 2, 3])
        get_total_goods_mock.side_effect = coroutine(lambda: 3)

        self.request("GET", "/goods/", params={'page': 10, 'items_on_page': 50})

        get_goods_id_list_mock.assert_called_once_with(page=10, items_on_page=50)

    @mock.patch('views.get_goods_id_list', autospec=True)
    @mock.patch('views.get_total_goods', autospec=True)
    def test_order_by(self, get_total_goods_mock, get_goods_id_list_mock):
        """
        :type get_goods_id_list_mock: unittest.mock.MagicMock
        """
        get_goods_id_list_mock.side_effect = coroutine(lambda order_by: [1, 2, 3])
        get_total_goods_mock.side_effect = coroutine(lambda: 3)

        self.request("GET", "/goods/", params={'order_by': 'name'})

        get_goods_id_list_mock.assert_called_once_with(order_by='name')


class GoodsOneInfoTestCase(MyBaseTestCase):
    @mock.patch('views.get_goods_one_info', autospec=True)
    def test_common(self, get_goods_one_info_mock):
        test_id = 123
        test_name = 'Товар 1'
        get_goods_one_info_mock.side_effect = coroutine(lambda id: {'id': test_id, 'name': test_name})

        content = self.request("GET", "/goods/{id}/".format(id=test_id))

        expected_data = {
            'id': test_id,
            'name': test_name,
        }
        self.assertEqual(content, expected_data)


class GoodsInfoTestCase(MyBaseTestCase):
    @mock.patch('views.get_goods_info', autospec=True)
    def test_common(self, get_goods_info_mock):
        test_id = 123
        test_name = 'Товар 1'
        get_goods_info_mock.side_effect = coroutine(lambda ids: [{'id': test_id, 'name': test_name}])

        content = self.request("GET", "/goods/info/", params={'ids': '123'})

        expected_data = {
            'items_info': [
                {
                    'id': test_id,
                    'name': test_name,
                }
            ]
        }
        self.assertEqual(content, expected_data)
        get_goods_info_mock.assert_called_once_with(ids=['123'])

    @mock.patch('views.get_goods_info', autospec=True)
    def test_id_list(self, get_goods_info_mock):
        get_goods_info_mock.side_effect = coroutine(lambda ids: [])

        content = self.request("GET", "/goods/info/", params={'ids': '1,2,3'})

        expected_data = {'items_info': []}
        self.assertEqual(content, expected_data)
        get_goods_info_mock.assert_called_once_with(ids=['1', '2', '3'])
