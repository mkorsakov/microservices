from aiohttp import web

from views import goods_view, goods_one_info, goods_get_info


def init_app(loop=None):
    app = web.Application(loop=loop)
    app.router.add_route('GET', r'/goods/', goods_view)
    app.router.add_route('GET', r'/goods/info/', goods_get_info)
    app.router.add_route('GET', r'/goods/{id}/', goods_one_info)
    return app
